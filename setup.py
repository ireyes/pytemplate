from os import path
from setuptools import setup, find_packages
from pytemplate import __version__

directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()

setup(
    name='pytemplate',
    version=__version__,
    description='Create the structure for Python projects',
    author='I. Reyes-Salazar',
    # author_email='',
    # license='',
    # url='',
    packages=find_packages(),  #  exclude from setup the packages that are not used by the this project directly. However, they are finally include in the manifest
    install_requires=required,
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'pytemplate = pytemplate.main:cli',
        ]
    }
)
