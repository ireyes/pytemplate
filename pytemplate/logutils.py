import logging
import sys
import tempfile
from logging import config as loggingconfig
from os import path

from pytemplate.utils import PyTemplateError, remove_file as rm


class InfoFilter(logging.Filter):
    def filter(self, rec):
        return rec.levelno in (logging.DEBUG, logging.INFO)


tmp_dir = tempfile.gettempdir()
log_file = path.join(tmp_dir, 'pytemplate.log')


def configure(debug):

    conf = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': '%(asctime)s -- %(name)s -- %(levelname)s: %(message)s'
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'filters': {
            'info': {
                '()': 'pytemplate.logutils.InfoFilter'
            }
        },
        'handlers': {
             'file': {
                'class': 'logging.FileHandler',
                'formatter': 'verbose' if debug else 'simple',
                'filename': log_file,
                'mode': 'w'  # ensure that it is created for each run
            }
        },
        'loggers': {
            'pytemplate': {
                'level': logging.DEBUG if debug else logging.INFO
            }
        },
        'root': {
            'level': 'WARNING',
            'handlers': ['file']
        }
    }
    loggingconfig.dictConfig(conf)


def remove_file():
    rm(log_file)


def exception_formatter(exception_type, exception, traceback, others_hook=sys.excepthook):
    print("An error occurred. Please, apologize. Please check the {} file for more information".format(log_file))
    others_hook(exception_type, exception, traceback)


def exception_formatter_reduced(exception_type, exception, traceback, others_hook=sys.excepthook):
    """
    Reduce verbosity of error messages associated with your exceptions

    Args:
        others_hook: hook for exceptions of a different class. Default is the system hook

    """
    print("An error occurred. Please, apologize us. Check the {} file for more information".format(log_file))
    if issubclass(exception_type, PyTemplateError):
        print("%s: %s" % (exception_type.__name__, exception), file=sys.stderr)
    else:
        others_hook(exception_type, exception, traceback)
