import sys
from os import path

import click


from pytemplate import logutils, project
from . import __version__

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.argument('template', metavar='<TEMPLATE>', type=click.Path())
@click.option('-d', '--description', default=False, is_flag=True, help='Get project description')
@click.option('--debug', default=False, is_flag=True, help='Give more output (verbose)')
@click.version_option(version=__version__)
def cli(template, description, debug):
    """Create a project from a <TEMPLATE>

    <TEMPLATE> indicates the folder where the template is

    You can use as <TEMPLATE>:

    \b
    - plain: for a simple python project
    - base: for a python project with some more options than plain (e.g. set up some logging options or a command line interaface)

    """
    logutils.configure(debug)
    if debug:
        sys.excepthook = logutils.exception_formatter
    else:
        sys.excepthook = logutils.exception_formatter_reduced

    if template == 'plain':
        dir_ = path.dirname(path.abspath(__file__))
        template_path = path.join(dir_, 'templates', 'plain')
    elif template == 'base':
        dir_ = path.dirname(path.abspath(__file__))
        template_path = path.join(dir_, 'templates', 'base')
    else:
        template_path = template

    if description:
        project.describe(template_path)
    else:
        project.build(template_path)
