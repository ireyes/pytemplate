Base project
============

Python basic project.

Its simple structure contains only the project directory a main script
and the main function. Additionally, it also comes with a simple :file:`setup.py` script,
a :file:`README.rst` file and a :file:`LICENSE.txt`.


Structure
---------

::

   project/
   |
   |-- project/
   |   |-- __init__.py
   |   |-- main.py
   |
   |-- LICENSE.txt
   |-- README.rst
   |-- setup.py
