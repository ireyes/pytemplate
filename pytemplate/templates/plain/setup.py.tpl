from os import path
from setuptools import setup, find_packages


setup(
    name='{{project.name}}',
    version='0.1',
    packages=find_packages(),
    install_requires=[]
)