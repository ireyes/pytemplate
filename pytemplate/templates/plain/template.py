import logging

from pytemplate.project import Project as Base


class Project(Base):

    def build(self, path):
        logging.getLogger(__name__).info('Path: {}'.format(path))
        super().build(path)
