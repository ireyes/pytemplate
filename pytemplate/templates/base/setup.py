from os import path
from setuptools import setup, find_packages
from {{project.name}} import __version__


directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


setup(
    name='{{project.name}}',
    version=__version__,
    description='',
    author_email='',
    license='',
    url='',
    packages=find_packages(),
    install_requires=required,
    {%- if project.cli %}
    entry_points={
        'console_scripts': [
            '{{project.cli}} = {{project.name}}.main:cli',
        ]
    }
    {%- endif %}
)