{% if project.logging == 'program' %}
import logging
from logging import config as loggingconfig
{%- endif %}

{%- if project.cli %}

import click


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
{%- endif %}

{%- if project.logging == 'program' %}


class LogFilter(logging.Filter):

    def filter(self, rec):
        return rec.levelno in (logging.DEBUG, logging.INFO)


def main(debug=False):

    logging_conf = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': '%(asctime)s -- %(name)s -- %(levelname)s: %(message)s'
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'filters': {
            'info': {
                '()': '{{project.name}}.main.LogFilter'
            }
        },
        'handlers': {
            'out': {
                'class': 'logging.StreamHandler',
                'formatter':'verbose' if debug else 'simple',
                'level': 'DEBUG',
                'stream': 'ext://sys.stdout',
                'filters': ['info']
            },
            'err': {
                'class': 'logging.StreamHandler',
                'formatter': 'verbose' if debug else 'simple',
                'level': 'WARNING',
                'stream': 'ext://sys.stderr',
            },
        },
        'loggers': {
            '{{project.name}}': {
                'level': logging.DEBUG if debug else logging.INFO
            }
        },
        'root': {
            'level': 'WARNING',
            'handlers': ['out', 'err']
        }
    }

    loggingconfig.dictConfig(logging_conf)
{%- else %}

def main():
    pass
{%- endif %}

{%- if project.cli %}


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('--debug', default=False, is_flag=True, help='Give more output (verbose)')
@click.version_option()
def cli(debug):
    """{{project.name}} help"""
    main({% if project.logging == 'program' %} debug{% endif %})
{%- endif %}


if __name__ == "__main__":
    main()
