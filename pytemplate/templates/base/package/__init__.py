__version__ = '0.1'
{%- if project.logging == 'library' %}
import logging
logging.getLogger(__name__).addHandler(logging.NullHandler())
{%- endif %}