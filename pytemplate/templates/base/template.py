
import click

from pytemplate.project import Project as BaseProject


class Project(BaseProject):

    def setup(self):
        super().setup()
        self.ask_logging()
        self.ask_cli()

        self.add_tip('Install the project in editable mode', cmd='pip install -e')

    def ask_logging(self):

        if click.confirm('Would you like to enable logging in your project?', default=False):
            if click.confirm('Is this project going to be a library?', default=False):
                self.logging = 'library'
            else:
                self.logging = 'program'

    def ask_cli(self):
        if click.confirm('Would you like to enable cli in your project?', default=False):
            self.cli = click.prompt('Under which command would you like to have the cli', default=self.name)

