Base project
============

Python basic project.

Its simple structure contains only the project directory a main script
and the main function. Additionally, it also comes with a simple :file:`setup.py` script,
a :file:`README.rst` file and a :file:`LICENSE.txt`.


Structure
---------

::

   project/
   |
   |-- project/
   |   |-- __init__.py
   |   |-- main.py
   |
   |-- LICENSE.txt
   |-- README.rst
   |-- setup.py


Additional features
-------------------

The :file:`setup.py` file can include additional parameters as the project description, url and author's email.

In addition it can also set up a basic logging system using Python ``logging`` library.
You can choose between setting up logging for a library (add the null handler)
or setting it up for a user program.