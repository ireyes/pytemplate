"""
Functions that use click to ask for parameters,
or that have some relation with click
"""

import os
import click

from pytemplate import utils


def check_empty_callback(ctx, param, value):
    if value.strip() == '':
        raise click.BadParameter('It cannot be empty')
    return value


def check_project_name(ctx, param, value):
    try:
        name = utils.check_project_name(value)
    except utils.ProjectNameError as e:
        raise click.BadParameter(e.message)
    else:
        return name


def ask_for_name():
    name = None
    while name is None:
        name = click.prompt('Name of the project')
        try:
            name = utils.check_project_name(name)
        except utils.ProjectNameError as e:
            print(e)
            name = None
    return name


def ask_for_path(name):
    """
    Get the path where to put the project

    Args:
        name (str): project name

    Returns:
        str. Path

    """
    path = None
    while path is None:
        path = click.prompt('Location for your project', type=click.Path(), default=os.path.join(os.getcwd(), name))
        if os.path.exists(path):
            print('Path already exist. To avoid overriding, provide a non existing path')
            path = None
    return path


def ask_for_cli(name):
    """
    Get the command under which the user wants his/her command line interface

    Args:
        name (str): project name used as default cmd

    Returns:
        str. Command name

    """
    if click.confirm('Would you like to have a command line interface (cli) in your project?', default=True):
        cmd = click.prompt('Under which command would you like to have the cli', default=name)
        return cmd
    return None

