"""
Utilities related to the project creation and the render of the files.

*.tpl* are considered the templates. The rendering is done using a
:class:`Project` object with is a class containing all the possible
names needed by the templates.
The final extension of the file should precede the *.tpl* extension.

In is the responsibility of the :file:`template.py` script of each
project template to create and fill the :class:`Project` object.
"""
import importlib
import os
import logging
import shutil
import sys
from os import path
from modulefinder import ModuleFinder

from jinja2 import Environment, FileSystemLoader

from pytemplate import logutils, click_utils
from pytemplate.tips import Tips
from pytemplate.utils import remove_file, PyTemplateError



def render_templates(src, dst, project):
    """
    Renders all templates in a folder

    Args:
        folder (str): path to a folder
        project (:class:`BGProject`): namespace with the variables used during the rendering process

    """
    logging.getLogger(__name__).info('Rendering templates')
    env = Environment(loader=FileSystemLoader(src))
    env.globals['project'] = project
    env.compile_templates(dst)


class ProjectError(PyTemplateError): pass


class Project:

    def __init__(self):
        self.tips = Tips()

    def setup(self):
        self.ask_name()
        self.ask_path()

    def teardown(self):
        self.tips.show()
        logutils.remove_file()

    def ask_name(self):
        self.name = click_utils.ask_for_name()

    def ask_path(self):
        self.path = click_utils.ask_for_path(self.name)

    # def ask(self):
    #     for meth in inspect.getmembers(self, predicate=inspect.ismethod):
    #         name = meth[0]
    #         if name.startswith('ask_'):
    #             meth[1]()

    def build(self, base_dir, unwanted_files=('__init__.py', 'template.py', 'project_doc.rst'), unwanted_dirs=('__pycache__', 'package/__pycache__')):
        """
        Creates the project from a template project

        Args:
            base_dir (str): path of the template project

        Workflow:
        #. Copy the template project
        #. Remove unnecessary files
        #. Rename files & folders
        #. Render the templates inside the project

        """

        print('Creating project files')

        render_templates(base_dir, self.path, self)

        logging.getLogger(__name__).info('Removing unwanted files')
        # remove files
        for file in unwanted_files:
            logging.getLogger(__name__).debug('Removing %s' % file)
            remove_file(os.path.join(self.path, file))

        for d in unwanted_dirs:
            logging.getLogger(__name__).debug('Removing %s' % d)
            shutil.rmtree(os.path.join(self.path, d), ignore_errors=True)

        logging.getLogger(__name__).info('Renaming package')
        # rename the package
        shutil.move(os.path.join(self.path, 'package'), os.path.join(self.path, self.name))



    def add_tip(self, msg, cmd=None):
        self.tips.add(msg, cmd)


def import_(path_):
    sys.path.insert(0, path_)
    try:
        module = importlib.import_module('template')
    except ModuleNotFoundError as e:
        if e.args[0] == "No module named 'template'":
            logging.getLogger(__name__).error('No module in %s' % path_)
            raise ProjectError('Missing template in {}'.format(path_)) from None
        else:
            finder = ModuleFinder([])
            finder.run_script(path_)
            missing_modules = ', '.join([m for m in finder.badmodules.keys() if m not in ['logging', 'os', 'click', 'pytemplate.project']])
            logging.getLogger(__name__).error('To run this template, you need to install the following packages %s' % missing_modules)
            raise ProjectError('These modules are missing: {}. Install them to build this template.'.format(missing_modules)) from None

    except ImportError as e:
        logging.getLogger(__name__).error('Error loading module in %s' % path)
        raise
        # try to import specified class name from specified module path, throw ImportError if this fails
    try:
        cls = getattr(module, 'Project')
    except AttributeError as e:
        logging.getLogger(__name__).error('Error loading class from module %s' % path)
        raise ImportError("Failed to import Project from %s: %s" % (path, e))  from None
    return cls


def build(template):
    cls = import_(template)
    project = cls()
    project.setup()
    project.build(template)
    project.teardown()


def describe(template):
    file = None
    for ext in ['', '.txt', '.rst', '.md']:
        f = path.join(template, 'project_doc' + ext)
        if path.isfile(f):
            file = f
            break
    if file is None:
        print('Description not found')
    else:
        with open(file) as fd:
            for line in fd:
                print(line, end='')
