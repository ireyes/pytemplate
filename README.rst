
pytemplate
==========

Utility to create the structure (the basics)
of a Python project.

Security warning
----------------

.. warning::

    When **pytemplate** gets a new package to build
    it inserts the location in the path and imports
    the ``template.py``. This can be a security risk
    when using untrusted sources because that file
    can contain anything. Please, use only trusted templates.

    This are the risky operations that **pytemplate** performs:

    .. code-block:: python

       sys.path.insert(0, path_to_template)
       module = importlib.import_module('template')


How it works
------------

**PyTemplates** receives the path to a directory with the template and loads it.
Currently, it needs to be a local path, but in the future in can be a remote location
accessible through different ways (FTP, HTTP...).

In that folder, a ``template.py`` indicates the build process,
which can be customized according to each template.
The way to customize your build is extending the ``Project`` class.
**pytemplate** will instantiate that class (with emtpy parameters)
and call 3 methods: *setup*, *build* and *teardown*.
All but *build* are called with empty paramerters.
*build* receives one parameter: the path to the folder with the template.

The goal of each method is:

- *setup*: get all the input from the user that is needed to build the package
- *build*: copy the files and render them
- *teardown*: do some cleaning and show tips to the user if any


What is already implemented
***************************

Currently, the built-in ``Project`` class will ask the user for a name
of the project and a path where to copy the files.
Then, it will copy all the files in the template folder to the
location and render all files ending in ``.tpl`` as **jinja2 templates**
and passing the whole project to the template, so all project data
attributes are accessible.
Then, undesired files are removed.

One extra feature is that if there is a folder
(inside the template folder) named **package**
it will be renamed as the project name.
The idea is the in a Python project the structure
is similar to::

    |- myprojectname/
    |
    |  |- myprojectname/
    |  |
    |- setup.py


Getting started
---------------

As an example, here we show the built-in *plain template*.
That template folder has this structure::

   |- plain/
   |  |- package/
   |  |  |- __init__.py
   |  |  |- main.py
   |  |- License.txt
   |  |- README.rst.tpl
   |  |- setup.py.tpl
   |  |- template.py

The ``template.py`` looks like:

.. code-block:: python

   import logging
   from pytemplate.project import Project as Base

    class Project(Base):

        def build(self, path):
            logging.getLogger(__name__).info('Path: {}').format(path)
            super().build(self.location)


As you can see, the template is nothing but the default with
logging message added.

This template, will copy that structure into a location
that the user decides and then it will render the templates (``.tpl``)
and rename the *package folder*.
In the end, you will see something like::


   |- projectname/
   |  |- projectname/
   |  |  |- __init__.py
   |  |  |- main.py
   |  |- License.txt
   |  |- README.rst
   |  |- setup.py

